#!/bin/sh

# http://www.savvyclutch.com/continuous-deployment-to-aws-ecs-and-circle-ci/
# Check that the environment variable has been set correctly

if ${BYPASS_S3:-false}; then
  exec "$@"
else
  if [ -z "$SECRETS_BUCKET_NAME" ]; then
    echo >&2 'error: missing SECRETS_BUCKET_NAME environment variable'
    exit 1
  fi

  # Output some debug info
  aws sts get-caller-identity

  # Load the S3 secrets file contents into the environment variables
  eval $(aws s3 cp s3://${SECRETS_BUCKET_NAME}/nativForm - | sed 's/^/export /')

  exec "$@"
fi
