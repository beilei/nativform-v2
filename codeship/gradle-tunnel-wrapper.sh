#!/bin/sh

#
# This script is used to set up a tunnel to the FFX Nexus server using stunnel
# and run a gradle task with that tunnel running.
#

TMPDIR=$(mktemp -d) || exit 1

setup()
{
  if ! [ -z $CI_NAME ] && [ ${CI_NAME:-x} = "codeship" ]; then
    git clone git@bitbucket.org:ffxoh/oh-kiwiops-codeship-scripts $TMPDIR/oh-kiwiops-codeship-scripts
    $TMPDIR/oh-kiwiops-codeship-scripts/init.sh
  fi
}

teardown()
{
  if ! [ -z $CI_NAME ] && [ ${CI_NAME:-x} = "codeship" ]; then
    rm -rf $TMPDIR/oh-kiwiops-codeship-scripts
  fi
}

abort()
{
  teardown
  exit 1
}

trap 'abort' 0
set -e

setup
./gradlew "$@"
teardown

trap : 0
exit 0
