# Starting from the Openjdk-8 container
FROM openjdk:8

# Set the WORKDIR. All following commands will be run in this directory.
WORKDIR /app

# Copying all gradle files necessary to install gradle with gradlew
COPY gradle gradle
COPY \
  build.gradle \
  gradlew \
  ./

# Install the gradle version used in the repository through gradlew
RUN ./gradlew -q --console plain

## uncomment this if you need to connect to the FFX nexus.
## Download stunnel so we can get back to Nexus
#RUN apt-get update && apt-get install -y stunnel && apt-get clean && rm -rf /var/lib/apt/lists/* /var/cache/*
#ARG PRIVATE_SSH_KEY
#ARG BITBUCKET_HOST_SSH_KEY
#
## Install SSH Key to connect to Bitbucket
#RUN mkdir -p "$HOME/.ssh" \
#  && echo $BITBUCKET_HOST_SSH_KEY >> $HOME/.ssh/known_hosts \
#  && echo $PRIVATE_SSH_KEY > $HOME/.ssh/id_rsa \
#  && chmod 400 $HOME/.ssh/id_rsa
#
## Install codeship helper scripts
#COPY codeship codeship
#
## Set up tunnel back to FFX Nexus, Run gradle resolveAllDependencies to install dependencies before adding the whole repository
#RUN CI_NAME=codeship codeship/gradle-tunnel-wrapper.sh --console plain resolveAllDependencies ; touch /tmp/dependenciesAlreadyResolved

# either dependencies are resolved ^above^, or we resolve them.
RUN [ -e /tmp/dependenciesAlreadyResolved ] || ./gradlew --console plain resolveAllDependencies
