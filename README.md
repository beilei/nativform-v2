# nativForm

## Template Build Notes
* You will need to supply the Cloudformation Scripts (in `../nativForm/src/main/cloudformation`)
  to the KiwiOps team.  Additionally, these scripts will probably need to be customized.
* Check through the generated sources (at `../nativForm`)
  for code which may need to be uncommented.  A list should appear after these
  notes.
    Of note is `src/main/cloudformation/app.yaml` - in the `Service` resource,
    there is an `AppIndex` property - there are plans to automate the
    generation of this, but for now, it will need to be set manually.
* Of course, you should replace this file with a real README.md

## Manual steps
* No git remote repository has been created.  You will need to do this and then
  `git remote add ...`
* No Codeship project has been created.  You will need to create this. 
  (Codeship Pro / docker build).
* Project's pipeline stack needs to be created.  Create the stack in 
  `src/main/cloudformation/pipeline.yaml` (or give this to KiwiOps and get them
  to create the pipeline stack).

## Deployment
* Push your code to `deploy-cfn-staging` to create the project's stack(s) in
  staging.
* Once the stack(s) is / are created, you can deploy the project by pushing to 
  `deploy-staging`.
* For production deployments, change `staging` in the branch name with
  `production`.
