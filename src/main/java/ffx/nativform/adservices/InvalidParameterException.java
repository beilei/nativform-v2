package ffx.nativform.adservices;

import lombok.Getter;

public class InvalidParameterException extends RuntimeException {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    @Getter
    private final String message;

    public InvalidParameterException(String message) {
        super();
        this.message = message;
    }

}
