package ffx.nativform.adservices.resource;

import java.util.List;
import javax.inject.Inject;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ffx.nativform.adservices.model.Feed;
import ffx.nativform.adservices.service.FeedService;
import ffx.nativform.adservices.service.FeedServiceException;


@Produces(MediaType.APPLICATION_JSON)
@RestController
public class FeedResource {

    @Inject
    private FeedService feedService;

    @GetMapping("/api/generatefeed")
    public List<Feed> generateFeed(
            @RequestParam(name = "timestamp", required = false) Long timestamp)
            throws FeedServiceException {
        return feedService.fetchAds(timestamp);

    }
}
