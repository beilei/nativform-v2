package ffx.nativform.adservices.resource;

import javax.inject.Inject;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import ffx.nativform.adservices.model.CapacityModel;
import ffx.nativform.adservices.model.Day;
import ffx.nativform.adservices.service.CapacityService;
import ffx.nativform.adservices.service.CapacityServiceException;

@Produces(MediaType.APPLICATION_JSON)
@RestController
public class CapacityResource {

	@Inject
	private CapacityService capacityService;

	@GetMapping("/api/generatecapacitymodel")
	public CapacityModel generateCapacityModel(@RequestParam(name = "day", required = true) Day dayOfWeek,
			@RequestParam(name = "range", required = true) int weekCount) throws CapacityServiceException {
		return capacityService.generateCapacityModel(dayOfWeek, weekCount);
	}

}
