package ffx.nativform.adservices.resource;

import javax.inject.Inject;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ffx.nativform.adservices.model.CampaignType;
import ffx.nativform.adservices.model.Simulation;
import ffx.nativform.adservices.service.SimulationService;


@Produces(MediaType.APPLICATION_JSON)
@RestController
public class SimulationResource {

    @Inject
    private SimulationService simulationService;

    @GetMapping("/api/simulation")
    public Simulation getSimulation(@RequestParam(name = "campaign_id") int campaignId,
            @RequestParam(name = "campaign_revision_id") int campRevisionId,
            @RequestParam(name = "type") CampaignType type) {
        return simulationService.getSimulation(campaignId, campRevisionId, type);
    }

    @PostMapping("/api/simulation")
    public Simulation scheduleSimulation(@RequestParam(name = "campaign_id") int campaignId,
            @RequestParam(name = "campaign_revision_id") int campRevisionId,
            @RequestParam(name = "type") CampaignType type) {
        return simulationService.scheduleSimulation(campaignId, campRevisionId, type);
    }
}
