package ffx.nativform.adservices.resource;

import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import ffx.nativform.adservices.ServiceResponse;
import ffx.nativform.adservices.SuccessServiceResponse;

/**
 * demonstrates a HTTP controller
 */

@Produces(MediaType.APPLICATION_JSON)
@RestController
public class HealthCheck {

    /**
     * Return a nice friendly message, JSON formatted.
     *
     * @return HTTP response JSON
     */
    @GetMapping("/api/healthcheck")
    public ResponseEntity<ServiceResponse> healthCheck() {
        // if application starts than all required resources have been initialized
        // properly. no need for extra validation;
        return ResponseEntity.status(HttpStatus.OK).body(SuccessServiceResponse.of());
    }

    @GetMapping("/api/testconnection")
    @Secured("ROLE_USER")
    public ResponseEntity<ServiceResponse> verifyAuthentication() {
        return ResponseEntity.status(HttpStatus.OK).body(SuccessServiceResponse.of());
    }

}
