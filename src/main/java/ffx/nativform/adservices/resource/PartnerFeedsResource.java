package ffx.nativform.adservices.resource;

import java.util.List;
import javax.inject.Inject;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.joda.time.DateTime;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ffx.nativform.adservices.model.PartnerFeed;
import ffx.nativform.adservices.service.PartnerFeedsService;

@Produces(MediaType.APPLICATION_JSON)
@RestController
public class PartnerFeedsResource {

    @Inject
    private PartnerFeedsService partnerFeedsService;

    @GetMapping("/api/partnerfeeds")
    public List<PartnerFeed> listPartnerFeeds(
            @RequestParam(name = "timestamp", required = false) Long timestamp) {

        Long currentTime = timestamp == null ? new DateTime().getMillis() : timestamp;
        return partnerFeedsService.loadIFrameUrls(currentTime);

    }

}
