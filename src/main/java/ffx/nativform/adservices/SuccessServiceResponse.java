package ffx.nativform.adservices;

public class SuccessServiceResponse extends ServiceResponse {

    public SuccessServiceResponse() {
        super(true);
    }
    
    private static SuccessServiceResponse response = new SuccessServiceResponse();

    public static SuccessServiceResponse of() {
        return response;
    }

}
