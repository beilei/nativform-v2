package ffx.nativform.adservices;

public class ConfigurationRuntimeException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    public ConfigurationRuntimeException() {
    }

    public ConfigurationRuntimeException(String message) {
        super(message);
    }

    public ConfigurationRuntimeException(Throwable cause) {
        super(cause);
    }

    public ConfigurationRuntimeException(String message, Throwable cause) {
        super(message, cause);
    }
}
