package ffx.nativform.adservices;

import org.springframework.core.convert.ConversionFailedException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import ffx.nativform.adservices.service.AdserivesServiceException;

@RestControllerAdvice
public class RestResponseEntityExceptionHandler {

	private static final String TYPE_MISMATCH_MESSAGE_FORMAT = "%s is not a valid %s. Required type is %s";

	@ExceptionHandler({ MethodArgumentTypeMismatchException.class })
	ResponseEntity<ApiError> handleInvalidReqeustException(MethodArgumentTypeMismatchException ex) {
		String message = null;
		if (ex.getCause() != null && ex.getCause() instanceof ConversionFailedException) {
			if (ex.getCause().getCause() != null && ex.getCause().getCause() instanceof InvalidParameterException) {
				message = ex.getCause().getCause().getMessage();
			}
		} else {
			String requiredType = ex.getRequiredType().getSimpleName();
			String actualValue = ex.getValue().toString();
			String fieldName = ex.getParameter().getParameterName();
			message = String.format(TYPE_MISMATCH_MESSAGE_FORMAT, actualValue, fieldName, requiredType);
		}

		ApiError apiError = new ApiError(HttpStatus.BAD_REQUEST, message);
		return new ResponseEntity<>(apiError, apiError.getStatus());
	}

	@ExceptionHandler(AdserivesServiceException.class)
	public ResponseEntity<ApiError> handleServiceException(AdserivesServiceException ex) {
		ApiError response = ApiError.of(ex);
		return new ResponseEntity<>(response, response.getStatus());
	}

	@ExceptionHandler(InvalidParameterException.class)
	public ResponseEntity<ApiError> handleServiceException(InvalidParameterException ex) {
		ApiError response = ApiError.of(ex);
		return new ResponseEntity<>(response, response.getStatus());
	}

	@ExceptionHandler(ConversionFailedException.class)
	public ResponseEntity<ApiError> handleServiceException(ConversionFailedException ex) {
		ApiError response = ApiError.of(ex);
		return new ResponseEntity<>(response, response.getStatus());
	}

}
