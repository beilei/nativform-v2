package ffx.nativform.adservices;

import org.springframework.core.convert.ConversionFailedException;
import org.springframework.http.HttpStatus;
import ffx.nativform.adservices.service.AdserivesServiceException;
import ffx.nativform.adservices.service.ExceptionOrigin;
import lombok.Getter;

public class ApiError extends ServiceResponse {
	private HttpStatus status;

	@Getter
	private String message;

	public ApiError(HttpStatus status, String message) {
		super(false);
		this.message = message;
		this.status = status;
	}

	public HttpStatus getStatus() {
		return status;
	}

	public static ApiError of(AdserivesServiceException ex) {
		return new ApiError(
				ex.getOrigin() == ExceptionOrigin.CLIENT ? HttpStatus.BAD_REQUEST : HttpStatus.INTERNAL_SERVER_ERROR,
				ex.getMessage());
	}

	public static ApiError of(InvalidParameterException ex) {
		return new ApiError(HttpStatus.BAD_REQUEST, ex.getMessage());
	}

	public static ApiError of(ConversionFailedException ex) {
		return new ApiError(HttpStatus.BAD_REQUEST, ex.getCause().getMessage());
	}

}
