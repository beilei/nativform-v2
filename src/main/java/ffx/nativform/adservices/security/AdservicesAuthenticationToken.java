package ffx.nativform.adservices.security;

import java.util.Arrays;
import org.springframework.security.authentication.AbstractAuthenticationToken;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = false)
public class AdservicesAuthenticationToken extends AbstractAuthenticationToken {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    private String authToken;

    public AdservicesAuthenticationToken(String authToken) {
        super(Arrays.asList(AdservicesGrantedAuthority.of()));
        this.authToken = authToken;
    }

    @Override
    public Object getCredentials() {
        return authToken;
    }

    @Override
    public Object getPrincipal() {
        return null;
    }

}
