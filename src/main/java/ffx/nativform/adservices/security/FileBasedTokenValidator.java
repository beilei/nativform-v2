package ffx.nativform.adservices.security;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.stereotype.Component;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class FileBasedTokenValidator implements TokenValidator {

    private final List<String> validTokens;

    public FileBasedTokenValidator(String filePath) throws IOException {
        log.debug("Loading token file from {}", filePath);
        Path tokenFile = Paths.get(filePath);
        validTokens = Files.readAllLines(tokenFile).stream().map(String::trim)
                .filter(line -> !line.startsWith("#")).filter(line -> line.length() >= 5)
                .collect(Collectors.toList());
    }

    @Override
    public boolean isValid(String xAuth) {
        return validTokens.contains(xAuth);
    }

}
