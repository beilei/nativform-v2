package ffx.nativform.adservices.security;

public interface TokenValidator {

    boolean isValid(String xAuth);

}
