package ffx.nativform.adservices.security;

import javax.inject.Inject;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.authentication.preauth.AbstractPreAuthenticatedProcessingFilter;

@Configuration
@EnableWebSecurity
public class AdServicesRestSecurityConfig extends WebSecurityConfigurerAdapter {

	@Inject
	private TokenAuthenticationProvider tokenAuthenticationProvider;
	@Inject
	private HeaderAuthenticationFilter tokenFilter;

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.authorizeRequests()
				.antMatchers("/api/testconnection", "/api/generatefeed", "/api/generatecapacitymodel",
						"/api/partnerfeeds", "/api/simulation")
				.authenticated().and().csrf().disable()
				.addFilterBefore(tokenFilter, AbstractPreAuthenticatedProcessingFilter.class);

	}

	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.authenticationProvider(tokenAuthenticationProvider);
	}
}
