package ffx.nativform.adservices.security;

import org.springframework.security.core.GrantedAuthority;

public class AdservicesGrantedAuthority implements GrantedAuthority {

    public static final String AUTHENTICATED_USER_ROLE = "USER";

    @Override
    public String getAuthority() {
        return AUTHENTICATED_USER_ROLE;
    }

    private static final AdservicesGrantedAuthority instance = new AdservicesGrantedAuthority();

    public static AdservicesGrantedAuthority of() {
        return instance;
    }

}
