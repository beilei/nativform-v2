package ffx.nativform.adservices;

import java.util.Locale;

import org.springframework.core.convert.converter.Converter;

import ffx.nativform.adservices.model.Day;

public class DayConverter implements Converter<String, Day> {

	@Override
	public Day convert(String source) {
		return Day.convert(source, Locale.getDefault());
	}

}
