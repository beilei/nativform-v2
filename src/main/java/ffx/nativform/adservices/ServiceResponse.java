package ffx.nativform.adservices;

import lombok.Data;

@Data
public class ServiceResponse {
    private boolean success;

    public ServiceResponse(boolean success) {
        super();
        this.success = success;
    }
}
