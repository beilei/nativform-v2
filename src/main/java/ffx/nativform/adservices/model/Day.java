package ffx.nativform.adservices.model;

import java.time.DayOfWeek;
import java.time.format.TextStyle;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import ffx.nativform.adservices.InvalidParameterException;
import lombok.Getter;

public enum Day {
	MON(0), TUE(1), WED(2), THUR(3), FRI(4), SAT(5), SUN(6);

	private static final ConcurrentMap<Locale, Map<Day, String[]>> DAY_NAMES = new ConcurrentHashMap<>();
	@Getter
	private final int index;

	private Day(int index) {
		this.index = index;
	}

	private static Map<Day, String[]> getDayNamesForLocale(Locale locale) {

		if (!DAY_NAMES.containsKey(locale)) {
			Map<Day, String[]> namesOfDay = new HashMap<>();
			Arrays.stream(Day.values()).forEach(dayOfWeek -> {
				DayOfWeek day = DayOfWeek.of(dayOfWeek.index + 1);
				namesOfDay.put(dayOfWeek, new String[] { day.getDisplayName(TextStyle.SHORT, locale).toUpperCase(),
						day.getDisplayName(TextStyle.FULL, locale).toUpperCase() });
			});
			DAY_NAMES.putIfAbsent(locale, namesOfDay);
		}
		return DAY_NAMES.get(locale);
	}

	public static Day convert(String parameter, Locale locale) {
		try {
			int intValue = Integer.parseInt(parameter);
			return convert(intValue);
		} catch (NumberFormatException e) {
			return convertToDayInWeek(parameter.toUpperCase(), locale);
		}
	}

	private static Day convertToDayInWeek(final String parameter, final Locale locale) {
		Map<Day, String[]> dayNames = getDayNamesForLocale(locale);

		Optional<Day> value = dayNames.entrySet().stream().filter(
				namedDay -> namedDay.getValue()[0].equals(parameter) || namedDay.getValue()[1].equals(parameter))
				.findFirst().map(Map.Entry::getKey);
		if (value.isPresent()) {
			return value.get();
		}

		throw new InvalidParameterException(
				String.format("The given weekday (%s) is in an unsupported format.", parameter));

	}

	public static Day convert(int index) {
		Optional<Day> dayInWeek = Arrays.stream(Day.values()).filter(day -> day.index == index).findFirst();
		if (dayInWeek.isPresent()) {
			return dayInWeek.get();
		}
		throw new InvalidParameterException(
				String.format("The given weekday %d was outside the valid range [0,6]", index));
	}

	public static String print(Day day, Locale locale) {
		return DayOfWeek.of(day.index + 1).getDisplayName(TextStyle.SHORT, locale);
	}

}
