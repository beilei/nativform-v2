package ffx.nativform.adservices.model;

import java.util.Arrays;
import java.util.Optional;

import ffx.nativform.adservices.InvalidParameterException;
import lombok.Getter;

public enum CampaignType {
	HYPO("hypothetical"), ACTUAL("actual");

	@Getter
	private final String type;

	private CampaignType(String type) {
		this.type = type;
	}

	public static CampaignType convert(final String name) {
		Optional<CampaignType> resolved = Arrays.stream(CampaignType.values())
				.filter(type -> type.getType().equals(name)).findFirst();
		if (resolved.isPresent()) {
			return resolved.get();
		}
		throw new InvalidParameterException(String.format("Type (%s) is not valid", name));
	}
}
