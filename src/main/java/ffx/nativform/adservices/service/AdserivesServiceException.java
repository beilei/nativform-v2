package ffx.nativform.adservices.service;

import lombok.Getter;

public class AdserivesServiceException extends Exception {

    @Getter
    private final ExceptionOrigin origin;

    public AdserivesServiceException(ExceptionOrigin origin, String message) {
        super(message);
        this.origin = origin;
    }

}
