package ffx.nativform.adservices.service;

import ffx.nativform.adservices.model.CampaignType;
import ffx.nativform.adservices.model.Simulation;

public interface SimulationService {

	Simulation getSimulation(int campaignId, int campRevisionId, CampaignType type);

	Simulation scheduleSimulation(int campaignId, int campRevisionId, CampaignType type);

}
