package ffx.nativform.adservices.service;

import java.util.List;

import ffx.nativform.adservices.model.PartnerFeed;

public interface PartnerFeedsService {

	public List<PartnerFeed> loadIFrameUrls(Long timestamp);
}
