package ffx.nativform.adservices.service;

import ffx.nativform.adservices.model.CapacityModel;
import ffx.nativform.adservices.model.Day;

public interface CapacityService {

    CapacityModel generateCapacityModel(Day day, int weekCount) throws CapacityServiceException;

}
