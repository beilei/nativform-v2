package ffx.nativform.adservices.service;

public enum ExceptionOrigin {

    CLIENT, SERVER;
}
