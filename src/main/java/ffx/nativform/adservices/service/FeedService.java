package ffx.nativform.adservices.service;

import java.util.List;
import ffx.nativform.adservices.model.Feed;

public interface FeedService {

    List<Feed> fetchAds(Long timestamp) throws FeedServiceException;

}
