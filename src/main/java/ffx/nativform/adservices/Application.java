package ffx.nativform.adservices;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Exists to launch SpringBoot framework only. The magic annotation @SpringBootApplication below
 * causes the application to search over all other Java classes to find routes and load them
 * automatically.
 */
@SpringBootApplication
public class Application {
  public static void main(String[] args) {
    SpringApplication.run(Application.class, args);
  }
}
