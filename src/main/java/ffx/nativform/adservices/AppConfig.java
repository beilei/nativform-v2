package ffx.nativform.adservices;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.format.support.FormattingConversionService;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

/**
 * The Configuration Uses Spring application.properties, CLI -D arguments and Environment variable
 * override functionality.
 */
@Configuration
@PropertySource("classpath:application.properties")
public class AppConfig extends WebMvcConfigurationSupport {


  @Value("${token.file.path}")
  private String tokenFilePath;


  /**
   * When spring loads the values into AppConfig, validate the properties passed in.
   */
  @EventListener(ContextRefreshedEvent.class)
  public void validate() {
    // do customized validation here
  }

  @Bean
  public String tokenFilePath() {
    return tokenFilePath;
  }

  @Override
  public FormattingConversionService mvcConversionService() {
    FormattingConversionService f = super.mvcConversionService();
    f.addConverter(new CampaignTypeConverter());
    f.addConverter(new DayConverter());
    return f;
  }
}
