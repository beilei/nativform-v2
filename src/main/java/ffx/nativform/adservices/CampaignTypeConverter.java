package ffx.nativform.adservices;

import org.springframework.core.convert.converter.Converter;
import ffx.nativform.adservices.model.CampaignType;

public class CampaignTypeConverter implements Converter<String, CampaignType> {

	@Override
	public CampaignType convert(String source) {
		return CampaignType.convert(source);
	}

}
