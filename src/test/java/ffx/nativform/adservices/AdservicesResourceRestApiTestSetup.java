package ffx.nativform.adservices;

import javax.inject.Inject;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import ffx.nativform.adservices.security.HeaderAuthenticationFilter;
import ffx.nativform.adservices.service.CapacityService;
import ffx.nativform.adservices.service.FeedService;
import ffx.nativform.adservices.service.PartnerFeedsService;
import ffx.nativform.adservices.service.SimulationService;

// @Configuration
@TestPropertySource("classpath:application-test.properties")
@AutoConfigureMockMvc
public class AdservicesResourceRestApiTestSetup {
	@MockBean
	FeedService feedService;
	@MockBean
	CapacityService capacityService;
	@MockBean
	PartnerFeedsService partnerFeedsService;

	@MockBean
	SimulationService simulationService;

	@Inject
	MockMvc mockMvc;

	RequestBuilder addSecurityHeader(MockHttpServletRequestBuilder mockHttpServletRequestBuilder) {
		return mockHttpServletRequestBuilder.header(HeaderAuthenticationFilter.AUTH_HEAD, "testing-token");
	}
}
