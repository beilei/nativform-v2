package ffx.nativform.adservices;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import ffx.nativform.adservices.security.HeaderAuthenticationFilter;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class HealthCheckrResourceTest extends AdservicesResourceRestApiTestSetup{


    @Test
    public void healthCheckShouldReturnSuccessIfAppLoaded() throws Exception {
        mockMvc.perform(get("/api/healthcheck")).andExpect(status().is2xxSuccessful())
                .andExpect(content().contentType("application/json;charset=UTF-8"))
                .andExpect(content().json("{\"success\":True}"));
    }

    @Test
    public void testConnectionWithoutAuthenticationShouldFail() throws Exception {
        mockMvc.perform(get("/api/testconnection")).andExpect(status().isForbidden());
    }

    @Test
    public void testConnectionWithValidAuthenticationHeaderShouldSuccess() throws Exception {
        mockMvc.perform(get("/api/testconnection").header(HeaderAuthenticationFilter.AUTH_HEAD,
                "testing-token")).andExpect(status().is2xxSuccessful());
    }

    @Test
    public void testConnectionWithInvalidAuthenticationHeaderShouldFail() throws Exception {
        mockMvc.perform(
                get("/api/testconnection").header(HeaderAuthenticationFilter.AUTH_HEAD, "random"))
                .andExpect(status().isForbidden());
    }
}
