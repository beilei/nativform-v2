package ffx.nativform.adservices;

import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import ffx.nativform.adservices.model.Day;
import ffx.nativform.adservices.security.HeaderAuthenticationFilter;
import ffx.nativform.adservices.service.CapacityServiceException;
import ffx.nativform.adservices.service.ExceptionOrigin;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class CapacityResourceRestApiTest extends AdservicesResourceRestApiTestSetup {

    private static final String CAPACITY_GENERATE_URI = "/api/generatecapacitymodel";

    @Test
    public void generateCapacityModelWithOutOfRangeLowerBoundWeekCountShouldFail()
            throws Exception {
        given(capacityService.generateCapacityModel(any(Day.class), eq(1)))
                .willThrow(new CapacityServiceException(ExceptionOrigin.CLIENT,
                        "Invalid range parameter; range is out of bounds."));
        mockMvc.perform(get(CAPACITY_GENERATE_URI).param("range", "1").param("day", "1")
                .header(HeaderAuthenticationFilter.AUTH_HEAD, "testing-token"))
                .andExpect(status().is4xxClientError()).andExpect(content().json(
                        "{\"success\":false, \"message\":\"Invalid range parameter; range is out of bounds.\"}"));
    }

    @Test
    public void generateCapacityModelWithOutOfRangeUpperBoundWeekCountShouldFail()
            throws Exception {
        given(capacityService.generateCapacityModel(any(Day.class), eq(6)))
                .willThrow(new CapacityServiceException(ExceptionOrigin.CLIENT,
                        "Invalid range parameter; range is out of bounds."));
        mockMvc.perform(get(CAPACITY_GENERATE_URI).param("range", "6").param("day", "1")
                .header(HeaderAuthenticationFilter.AUTH_HEAD, "testing-token"))
                .andExpect(status().is4xxClientError()).andExpect(content().json(
                        "{\"success\":false, \"message\":\"Invalid range parameter; range is out of bounds.\"}"));
    }

    @Test
    public void generateCapacityModelWithInvalidDayRepresentationShouldFail() throws Exception {
        mockMvc.perform(get(CAPACITY_GENERATE_URI).param("range", "4").param("day", "something")
                .header(HeaderAuthenticationFilter.AUTH_HEAD, "testing-token"))
                .andExpect(status().is4xxClientError()).andExpect(content().json(
                        "{\"success\":false, \"message\":\"The given weekday (SOMETHING) is in an unsupported format.\"}"));
    }


    @Test
    public void generateCapacityModelWithDayInNnumberShouldSucceed() throws Exception {
        mockMvc.perform(get(CAPACITY_GENERATE_URI).param("range", "4").param("day", "1")
                .header(HeaderAuthenticationFilter.AUTH_HEAD, "testing-token"))
                .andExpect(status().is2xxSuccessful());
    }


    @Test
    public void generateCapacityModelWithDayInValid3LettersProperCaseShouldSucceed()
            throws Exception {
        mockMvc.perform(get(CAPACITY_GENERATE_URI).param("range", "4").param("day", "Mon")
                .header(HeaderAuthenticationFilter.AUTH_HEAD, "testing-token"))
                .andExpect(status().is2xxSuccessful());
    }

    @Test
    public void generateCapacityModelWithDayInValid3LettersUpperCaseShouldSucceed()
            throws Exception {
        mockMvc.perform(get(CAPACITY_GENERATE_URI).param("range", "4").param("day", "TUE")
                .header(HeaderAuthenticationFilter.AUTH_HEAD, "testing-token"))
                .andExpect(status().is2xxSuccessful());
    }

    @Test
    public void generateCapacityModelWithDayInValid3LettersLowerCaseShouldSucceed()
            throws Exception {
        mockMvc.perform(get(CAPACITY_GENERATE_URI).param("range", "4").param("day", "wed")
                .header(HeaderAuthenticationFilter.AUTH_HEAD, "testing-token"))
                .andExpect(status().is2xxSuccessful());
    }

    @Test
    public void generateCapacityModelWithDayInValid3LettersMixedCaseShouldSucceed()
            throws Exception {
        mockMvc.perform(get(CAPACITY_GENERATE_URI).param("range", "4").param("day", "ThU")
                .header(HeaderAuthenticationFilter.AUTH_HEAD, "testing-token"))
                .andExpect(status().is2xxSuccessful());
    }

    @SuppressWarnings("unchecked")
    @Test
    public void generateCapacityModelServiceException() throws Exception {
        given(this.capacityService.generateCapacityModel(Day.THUR, 4))
                .willThrow(CapacityServiceException.class);
        mockMvc.perform(get(CAPACITY_GENERATE_URI).param("range", "4").param("day", "ThU")
                .header(HeaderAuthenticationFilter.AUTH_HEAD, "testing-token"))
                .andExpect(status().is5xxServerError());
    }
}
