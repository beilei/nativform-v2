package ffx.nativform.adservices;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class PartnerFeedsResourceRestApiTest extends AdservicesResourceRestApiTestSetup {

  private static final String PARNTER_FEEDS_URI = "/api/partnerfeeds";

  @Test
  public void listPartnerFeedsWithNoTimestampGetDefaultTimestamp() throws Exception {
    mockMvc.perform(addSecurityHeader(get(PARNTER_FEEDS_URI)))
        .andExpect(status().is2xxSuccessful());
  }

  @Test
  public void postMethodIsNotAccepted() throws Exception {
    mockMvc.perform(addSecurityHeader(post(PARNTER_FEEDS_URI)))
        .andExpect(status().is4xxClientError());
  }


}
