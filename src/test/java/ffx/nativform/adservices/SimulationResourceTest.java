package ffx.nativform.adservices;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.only;
import ffx.nativform.adservices.model.CampaignType;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class SimulationResourceTest extends AdservicesResourceRestApiTestSetup {
	private static final String SIMULATION_URI = "/api/simulation";

	@Test
	public void scheduleSimulationWithPostMethod() throws Exception {
		mockMvc.perform(addSecurityHeader(post(SIMULATION_URI).param("type", CampaignType.ACTUAL.getType())
				.param("campaign_id", "1").param("campaign_revision_id", "1"))).andExpect(status().is2xxSuccessful());
		verify(simulationService, only()).scheduleSimulation(1, 1, CampaignType.ACTUAL);
	}

	@Test
	public void scheduleSimulationWithInvalidType() throws Exception {
		mockMvc.perform(addSecurityHeader(post(SIMULATION_URI).param("type", "random")))
				.andExpect(status().is4xxClientError());
	}

	@Test
	public void getSimulationWithInvalidType() throws Exception {
		mockMvc.perform(addSecurityHeader(get(SIMULATION_URI).param("type", "random")))
				.andExpect(status().is4xxClientError());
	}

	@Test
	public void getSimulationWithProperParameters() throws Exception {
		mockMvc.perform(addSecurityHeader(get(SIMULATION_URI).param("type", CampaignType.HYPO.getType())
				.param("campaign_id", "1").param("campaign_revision_id", "1"))).andExpect(status().is2xxSuccessful());
		verify(simulationService, only()).getSimulation(1, 1, CampaignType.HYPO);
	}
}
