package ffx.nativform.adservices;

import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import org.joda.time.DateTime;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import ffx.nativform.adservices.security.HeaderAuthenticationFilter;
import ffx.nativform.adservices.service.ExceptionOrigin;
import ffx.nativform.adservices.service.FeedServiceException;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class FeedResourceRestApiTest extends AdservicesResourceRestApiTestSetup {
    private static final String FEED_URI = "/api/generatefeed";

    @Test
    public void generateFeedWithNoTimestampGetDefaultTimestamp() throws Exception {
        mockMvc.perform(addSecurityHeader(get(FEED_URI))).andExpect(status().is2xxSuccessful());
    }


    @Test
    public void generateFeedRequestWithoutTokenShouldFail() throws Exception {
        mockMvc.perform(get(FEED_URI)).andExpect(status().isForbidden());
    }

    @Test
    public void generateFeedWithNoNumberTimestampShouldFail() throws Exception {
        mockMvc.perform(get(FEED_URI).param("timestamp", "randomString")
                .header(HeaderAuthenticationFilter.AUTH_HEAD, "testing-token"))
                .andExpect(status().is4xxClientError()).andExpect(content().json(
                        "{\"success\":false, \"message\":\"randomString is not a valid timestamp. Required type is Long\"}"));
    }


    @Test
    public void generateFeedWithFutureTimestampShouldFail() throws Exception {
        Long futureTime = DateTime.now().plusMonths(1).getMillis();
        given(this.feedService.fetchAds(futureTime))
                .willThrow(new FeedServiceException(ExceptionOrigin.CLIENT, "Future timestamp"));
        mockMvc.perform(get(FEED_URI).header(HeaderAuthenticationFilter.AUTH_HEAD, "testing-token")
                .param("timestamp", futureTime + "")).andExpect(status().is4xxClientError())
                .andExpect(content().json("{\"success\":false, \"message\":\"Future timestamp\"}"));
    }
}
