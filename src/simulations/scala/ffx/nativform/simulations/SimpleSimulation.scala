package ffx.nativform.simulations

import io.gatling.core.Predef._
import io.gatling.http.Predef._

import org.springframework.boot.SpringApplication
import org.springframework.boot.context.embedded.EmbeddedWebApplicationContext
import org.springframework.boot.context.embedded.tomcat.TomcatEmbeddedServletContainer

import ffx.nativform.adservices.Application

class SimpleSimulation extends Simulation {

  val headers = Map("Accept" -> """application/json""")

  val healthCheckPage = repeat(5) {
    exec(http("healthcheck of system")
      .get("/api/healthcheck"))
      .pause(2, 2)
  }

  val scn = scenario("Health check the system")
    .exec(healthCheckPage)


  System.setProperty("spring.profiles.default", System.getProperty("spring.profiles.default", "it"));  

  // Launch the "unit under test", on a random port
  val app = SpringApplication.run(classOf[Application], "--server.port=0")

  // extract the port number
  val esc = app.asInstanceOf[EmbeddedWebApplicationContext].getEmbeddedServletContainer()
  val port = esc.asInstanceOf[TomcatEmbeddedServletContainer].getPort()

  // construct the endpoint
  val baseUrl = "http://localhost:" + port;
  val httpConf = http.baseURL(baseUrl)  

  // add shutdown hook
  Runtime.getRuntime.addShutdownHook(new Thread() {
    override def run(): Unit = app.stop()
  })

  // run the test.
  setUp(scn.inject(atOnceUsers(5)).protocols(httpConf)).assertions(
    global.successfulRequests.percent.gte(99))
}
