FROM openjdk:7u121-jre-alpine
MAINTAINER kiwiops@fairfaxmedia.co.nz
# Install AWS CLI
RUN \
	mkdir -p /aws && \
	apk -Uuv add groff jq less python py-pip && \
	pip install awscli && \
	apk --purge -v del py-pip && \
	rm /var/cache/apk/*

WORKDIR /opt/nativForm
COPY build/libs/nativForm-0.1.0.jar .
COPY ecs-entrypoint.sh /ecs-entrypoint.sh
ENTRYPOINT ["/ecs-entrypoint.sh"]
ENV JAVA_OPTS " "
CMD ["sh", "-c", "java $JAVA_OPTS -jar nativForm-0.1.0.jar"]
EXPOSE 8080
